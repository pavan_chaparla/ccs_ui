import { Injectable } from '@angular/core';
import { Router } from  '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, Output, EventEmitter } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from './user.model'; 

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  user$: Observable<any>;
  email:string
  
  @Output() messageEvent = new EventEmitter<string>();
  constructor(private afs : AngularFirestore, private afAuth: AngularFireAuth,private router: Router ) { 
      this.user$ = this.afAuth.authState.pipe(
        switchMap(user => {
          if(user){
            return this.afs.doc<User>('users/${user.uid}').valueChanges();
          }
          else{
            return of(null);
          }
        })
      )
  }
  async googleSignin(){
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    this.email = await credential.user.email
    return this.router.navigate(['/posdata']);
  }
  async signOut() {
    await this.afAuth.auth.signOut();
    return this.router.navigate(['/']);
  }
  sendMessage() {
    this.messageEvent.emit(this.email)
  }
}
