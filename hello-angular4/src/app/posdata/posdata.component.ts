import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-posdata',
  templateUrl: './posdata.component.html',
  styleUrls: ['./posdata.component.css']
})
export class PosdataComponent implements OnInit {
  Minutes = [];
  Hours = [];

  constructor( public auth: AuthService , private http_client: HttpClient, private flashMessage: FlashMessagesService) {
    for(var i=0 ; i<60; i++){
      this.Minutes[i]=i;
    }
    for(var i=1 ; i<13; i++){
      this.Hours[i]=i;
    }
   }

  ngOnInit() {
  }
  submit(){
    var date = (<HTMLInputElement>document.getElementById("Date")).value;
    var minutes =  (<HTMLInputElement>document.getElementById("Minutes")).value;
    var hours =  (<HTMLInputElement>document.getElementById("Hours")).value;
    var session =  (<HTMLInputElement>document.getElementById("Session")).value;
    var min = parseInt(minutes);
    console.log(hours)
    if(min<10){
      minutes = "0"+minutes;
    }
    console.log(minutes)
    console.log(date)
    var hour = parseInt(hours);
    if(hour<10){
      hours = "0"+hours;
    }
    var date_time = date+" "+hours+":"+minutes+" "+session;    
    var api_body = {
      date_time: date_time,
      email: this.auth.email
    }
    var url = "http://35.243.169.39/api/process_pos_data"
    this.http_client.post(url,JSON.stringify(api_body)).subscribe(res =>{
      console.log("This is response",res)
    });
    this.flashMessage.show('Your Inputs are processed and the Videos are retrieved and sent to your drive', { cssClass: 'alert-success', timeout: 4000 });
  }
}
