import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosdataComponent } from './posdata.component';

describe('PosdataComponent', () => {
  let component: PosdataComponent;
  let fixture: ComponentFixture<PosdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
