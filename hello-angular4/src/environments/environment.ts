// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyCD0PAt59z6XylW04Gk8kgPUrnB2G4oYjY",
    authDomain: "custom-cloud-solutions.firebaseapp.com",
    databaseURL: "https://custom-cloud-solutions.firebaseio.com",
    projectId: "custom-cloud-solutions",
    storageBucket: "",
    messagingSenderId: "991726756954",
    appId: "1:991726756954:web:2e8af129d265b68803a463"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
