import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PosdataComponent } from './posdata/posdata.component'
import { HomeComponent } from './home/home.component'



const routes: Routes = [
  {path:'posdata',component:PosdataComponent},
  {path:'',component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
